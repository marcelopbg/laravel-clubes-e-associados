<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/socio');
});
Route::get('associacao/{id}/{socioId}', 'SocioController@deleteAssociacao');
Route::get('clube/search/', 'ClubeController@search');  

Auth::routes();
Route::resource('socio', 'SocioController');
Route::resource('clube', 'ClubeController');
