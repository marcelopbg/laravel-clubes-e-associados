<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Socio extends Model
{

	public function associacao()
	{
		return $this->hasOne('App\Socio');
	}
}
