<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Associacao extends Model
{
    public function socio()
	{
		return $this->hasMany('App\Associacao');
	}
	public function clube()
	{
		return $this->hasMany('App\Associacao');
	}
}
