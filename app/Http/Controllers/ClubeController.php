<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clube;
use App\Socio;

class ClubeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
   
    /**
     * @param  \Illuminate\Http\Request  $request
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      echo($request->content);
      $registros = Clube::all();
       return view('clube.list',['registros' => $registros]);
    }
        /**
     * @param  \Illuminate\Http\Request  $request
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
      echo($request->content);
      $registros = Clube::where('nome','like',$request->search.'%')->get();
       return view('clube.list',['registros' => $registros]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $socios = Socio::all();
       return view('clube.create', ['socios' => $socios]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {       
       $this->validate($request,['nome'=> 'required',]);
       
       $registro = new Clube;
       $registro->nome = $request->nome;
       
       $registro->save();
       return redirect()->route('clube.index')->with('alert-success','Registro salvo com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $registro = Clube::findOrFail($id);
       return view('clube.edit',['registro' => $registro]);
 }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $this->validate($request,['nome'=> 'required',]);
       
       $registro = Clube::findOrFail($id);
       
       $registro->nome = $request->nome;
       $registro->save();
       
       return redirect()->route('clube.index')->with('alert-success','Registro salvo com sucesso!');
    }

  /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
       
       $registro = Associacao::findOrFail($id);
       $registro->delete();
       
       return redirect()->route('clube.index')->with('alert-success','Registro salvo com sucesso!');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $registro = Clube::findOrFail($id);
        $registro->delete();
        return redirect()->back()->with('alert-success','Registro excluído com sucesso!');
    }
}
