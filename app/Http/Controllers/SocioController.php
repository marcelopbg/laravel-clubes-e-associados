<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Clube;
use App\Socio;
use App\Associacao;

class SocioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $registros = Socio::all();
       $clubes = Db::table('associacaos')->join('clubes', 'clubes.id', '=', 'associacaos.clube_id')->get();
 
       return view('socio.list',['registros' => $registros], ['clubes' => $clubes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $socios = Socio::all();
       return view('socio.create', ['socios' => $socios]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {       
       $this->validate($request,['nome'=> 'required',]);
       
       $registro = new Socio;
       $registro->nome = $request->nome;
       
       $registro->save();
       return redirect()->route('socio.index')->with('alert-success','Registro salvo com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteAssociacao($id, $socioId)
    {
      $registro = Associacao::findOrFail($id);
      $registro->delete();
      return redirect()->back()->with('alert-success','Registro excluído com sucesso!');        
        return $test;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $registro = Socio::findOrFail($id);
       $clubes = Clube::all();
       $associacoes = Associacao::select('associacaos.id', 'associacaos.clube_id', 'associacaos.socio_id', 'clubes.nome')->where('associacaos.socio_id', '=',$id)->join('clubes', 'clubes.id', '=', 'associacaos.clube_id')->get();
       return view('socio.edit',['registro' => $registro, 'clubes' =>$clubes, 'associacoes' =>$associacoes]);
 }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $this->validate($request,['nome'=> 'required','novoClubeId']);
       
       $registro = Socio::findOrFail($id);
       
       $registro->nome = $request->nome;
       $registro->save();
       if($request->novoClubeId) {

          if(Associacao::where([
             ['associacaos.socio_id', '=',$id],
             [ 'associacaos.clube_id', '=', $request->novoClubeId]
             ])->doesntExist()
          ) {
      $associacao = new Associacao;
      $associacao->clube_id = $request->novoClubeId;
      $associacao->socio_id = $id;
      $associacao->save();
      }
      }
      
       return redirect()->route('socio.index')->with('alert-success','Registro salvo com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $registro = Socio::findOrFail($id);
        $registro->delete();
        return redirect()->back()->with('alert-success','Registro excluído com sucesso!');
    }  
}
