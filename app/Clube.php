<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clube extends Model
{
    public function associacao()
	{
		return $this->hasOne('App\Clube');
	}
}
