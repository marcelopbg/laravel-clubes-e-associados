@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Edição de Sócio</div><br>

                <div class="panel-body">
                <form role="form" action="{{route('socio.update', $registro->id)}}" method="post">
                     <div class="box-body">
                  <input name="_method" type="hidden" value="PATCH">
                        {{csrf_field()}}

                        <div class="form-group col-sm-8">
                           <label for="nome">Nome</label>
                           <input type="text" class="form-control" name="nome" placeholder="Informe o nome da empresa" value="{{$registro->nome}}" required>
                        </div>
                        <div class="form-group col-sm-8">
                         <label for="novoClubeId">
                            Adicionar Clube:
                            </label>
                            <select class="custom-select mr-sm-1" name="novoClubeId">
                            <option value="" disabled selected></option>
                            @foreach($clubes as $clube)     
                            <option value="{{$clube->id}}">
                            {{$clube->nome}}    
                            </option>
                            @endforeach                        
                          </select>
                         </div>
                         <!-- /.input group -->
                     </div>
                     <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Salvar</button>
                       <a href="{{route('socio.index')}}" class="btn btn-default pull-right">Cancelar</a>
                    </div>
                  </form>
               <br><br>
                </div>   
                </div>
                </div>      
            <div class="col-lg-6">
            <div class="panel panel-default">
            <div class="panel-heading">Clubes associados</div><br>
            <div class="panel-body">
        <div class="box-body row ml-3">
        @foreach($associacoes  as $associacao)
        <div>
                <a href="../../associacao/{{$associacao->id}}/{{$associacao->socio_id}}" class="badge badge-danger mr-2" style="float:right">x</a>

        <button id="disabled" disabled href="" class="btn btn-success btn-sm mb-2" style="background-color: #38c172; opacity: 100; ">{{$associacao->nome}} 
        </button>
        </div>
        @endforeach
        </div>
        </div>
        </div>
    </div>  
        </div>
     
</div>
@endsection
