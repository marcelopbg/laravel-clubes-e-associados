@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Sócios  </div>

                <div class="panel-body">
                  <table class="table table-hover">
                  <thead>
                     <tr>
                        <th>Nome</th>
                        <th>Clubes</th>

                     </tr>
                  </thead>
                  <tbody>
                     @foreach($registros as $registro)
                     <td>{{$registro->nome}}</td>
                     <td>
                     @foreach($clubes as $clube)
                     @if($registro->id==$clube->socio_id)
                   <li>  {{$clube->nome}} </li>
                     @endif   
                     @endforeach
                   </td>
                     <td>                   
                           <form class="" action="{{route('socio.destroy',$registro->id)}}" method="post">
                              <input type="hidden" name="_method" value="delete">
                              
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                              <a href="{{route('socio.edit',$registro->id)}}" class="btn btn-primary btn-sm">Editar</a>
                              
                              <input type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Você tem certeza que deseja excluir este registro?');" name="name" value="Excluir">
                           </form>
                        </td>
                      
                     </tr>
                     @endforeach
                     </td>
                  </tbody>
                  <tfoot>
                  </tfoot>
               </table>
               <a href="{{route('socio.create')}}" class="btn btn-primary">Novo Sócio</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
