@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edição do clube</div>

                <div class="panel-body">
                    
                   <form role="form" action="{{route('clube.update', $registro->id)}}" method="post">
                     <div class="box-body">
                  <input name="_method" type="hidden" value="PATCH">
                        {{csrf_field()}}

                        <div class="form-group col-sm-8">
                           <label for="nome">Nome</label>
                           <input type="text" class="form-control" name="nome" placeholder="Informe o nome da empresa" value="{{$registro->nome}}" required>
                        </div>
                         <!-- /.input group -->
                     </div>
                     <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Salvar</button>
                       <a href="{{route('clube.index')}}" class="btn btn-default pull-right">Cancelar</a>
                    </div>
                  </form>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
