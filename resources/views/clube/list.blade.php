@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Clubes  </div>
                <div class="panel-body">
                   <form method="GET" action="{{url('clube/search')}}">
                   <div class="row">
                  <div class="col-sm-12">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="_method" value="get">

                   <input name="search" type="text" class="">
                   <button type="submit" name="name" class="btn btn-primary btn-sm">Buscar</button>
                   </div>
               </div>
               </form>
                  <table class="table table-hover">
                  <thead>
                     <tr>
                        <th>Nome</th>
                     </tr>
                  </thead>
                  <tbody>
               

                     @foreach($registros as $registro)
                     <td>{{$registro->nome}}</td>
                     <td>                   
                           <form class="" action="{{route('clube.destroy',$registro->id)}}" method="post">
                              <input type="hidden" name="_method" value="delete">
                              
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                              <a href="{{route('clube.edit',$registro->id)}}" class="btn btn-primary btn-sm">Editar</a>
                              
                              <input type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Você tem certeza que deseja excluir este registro?');" name="name" value="Excluir">
                           </form>
                        </td>
                     </tr>
                     @endforeach
                     </td>
                  </tbody>
                  <tfoot>
                  </tfoot>
               </table>
               <a href="{{route('clube.create')}}" class="btn btn-primary">Novo Clube</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
