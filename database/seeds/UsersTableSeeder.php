<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $socio = new \App\Socio();
        $clube = new \App\Clube();
        $associacao = new \App\Associacao();
        $user = new \App\User();
		$user->name = 'Marcelo P. B. Guimarães';
		$user->email = 'mpbguima@gmail.com';
		$user->password = bcrypt('123123');
		$user->save();
    }
}
