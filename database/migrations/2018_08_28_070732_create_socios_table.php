<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSociosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('socios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->timestamps();
        });
        Schema::create('associacaos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('clube_id')->unsigned()->default(0);
            $table->foreign('clube_id')->
              references('id')->on('clubes')->
              onDelete('cascade');
            $table->integer('socio_id')->unsigned()->default(0);
            $table->foreign('socio_id')->
              references('id')->on('socios')->
              onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('socios');
        Schema::dropIfExists('associacaos');

    }
}
